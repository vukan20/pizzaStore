package com.store.validator;

import com.store.dto.PizzaDTO;
import com.store.repository.PizzaRepository;
import com.wo.exception.ErrMessages;
import com.wo.exception.SystemException;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class PizzaValidator {

  private PizzaRepository pizzaRepository;

  public PizzaValidator(PizzaRepository pizzaRepository) {
    this.pizzaRepository = pizzaRepository;
  }

  public void validateSaveUpdate(PizzaDTO pizzaDTO) {
    checkMandatoryFields(pizzaDTO);
    checkMinimalPrice(pizzaDTO);
    checkUniqueSlug(pizzaDTO);
    checkSlugFormat(pizzaDTO);
  }

  public void validateSlug(PizzaDTO pizzaDTO) {
    checkSlugFormat(pizzaDTO);
    checkUniqueSlug(pizzaDTO);
  }

  private void checkMandatoryFields(PizzaDTO pizzaDTO) {
    if (pizzaDTO.getName() == null || pizzaDTO.getSlug() == null
            || pizzaDTO.getPrice() == null || pizzaDTO.getSize() == null) {
      throw new SystemException(ErrMessages.REQUIRED_FIELD + ": "
              + (pizzaDTO.getName() == null ? "name" : "") + (pizzaDTO.getSlug() == null ? "slug" : "")
              + (pizzaDTO.getPrice() == null ? "price" : "") + (pizzaDTO.getSize() == null ? "size" : ""));
    }
  }

  private void checkMinimalPrice(PizzaDTO pizzaDTO) {
    if (pizzaDTO.getPrice().compareTo(new BigDecimal(0.01)) < 0) {
      throw new SystemException(ErrMessages.MINIMAL_PRICE);
    }
  }

  private void checkUniqueSlug(PizzaDTO pizzaDTO) {
    if (pizzaDTO.getId() != null) {
      if (pizzaRepository.existsBySlugAndIdNot(pizzaDTO.getSlug(), pizzaDTO.getId())) {
        throw new SystemException(ErrMessages.UNIQUE_SLUG);
      }
    } else {
      if (pizzaRepository.existsBySlug(pizzaDTO.getSlug())) {
        throw new SystemException(ErrMessages.UNIQUE_SLUG);
      }
    }

  }

  private void checkSlugFormat(PizzaDTO pizzaDTO) {
    if (!isStringLowerCase(pizzaDTO.getSlug())) {
      throw new SystemException(ErrMessages.INVALID_SLUG_FORMAT);
    }
  }

  private boolean isStringLowerCase(String str) {
    char[] charArray = str.toCharArray();

    for (int i = 0; i < charArray.length; i++) {
      if (!Character.isLowerCase(charArray[i]))
        return false;
    }

    return true;
  }

}
