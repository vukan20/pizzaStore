package com.store.service.impl;

import com.store.dto.PizzaDTO;
import com.store.dto.search.PizzaSearch;
import com.store.entity.PizzaBean;
import com.store.repository.PizzaRepository;
import com.store.service.PizzaService;
import com.store.validator.PizzaValidator;
import com.wo.dto.SearchResultDTO;
import com.wo.service.impl.BaseServiceImpl;
import com.wo.service.search.Pager;
import com.wo.service.search.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class PizzaServiceImpl extends BaseServiceImpl<PizzaBean, Long> implements PizzaService {

  private Logger logger = LoggerFactory.getLogger(this.getClass());

  private PizzaRepository pizzaRepository;

  private PizzaValidator pizzaValidator;

  public PizzaServiceImpl(PizzaRepository pizzaRepository, PizzaValidator pizzaValidator) {
    super();
    this.pizzaRepository = pizzaRepository;
    this.pizzaValidator = pizzaValidator;
  }

  @Override
  public SearchResultDTO<PizzaDTO> searchDTO(PizzaSearch search, Pager pager, Set<String> ignoreFields) {
    SearchResult<PizzaBean> all = search(search, pager);

    SearchResultDTO<PizzaDTO> results = new SearchResultDTO<>(all);
    results.setResults(all.getResults().stream().map(PizzaDTO::new).collect(Collectors.toList()));

    return results;
  }

  @Override
  @Cacheable(cacheNames = "getPizza")
  public PizzaDTO getDTO(Long id) {
    logger.info("Getting Pizza object with id: " + id);

    Optional<PizzaBean> pizza = pizzaRepository.findById(id);

    pizza.orElseThrow(() -> new IllegalStateException("Pizza object with id: " + id + " not found!"));
    return mapToDTO(pizza.get());
  }

  @Override
  @CacheEvict(value = "savePizza", key = "#dto", allEntries = true)
  public PizzaDTO saveDTO(PizzaDTO dto) {
    logger.info("Saving Pizza object!");
    // pizzaValidator.validateSaveUpdate(dto);
    pizzaValidator.validateSlug(dto);
    PizzaBean pizza = new PizzaBean();
    dto.setDate(new Date());
    BeanUtils.copyProperties(dto, pizza);
    return mapToDTO(save(pizza));
  }

  @Override
  @CacheEvict(value = "updatePizza", key = "#dto.id", allEntries = true)
  public PizzaDTO updateDTO(PizzaDTO dto) {
    logger.info("Update Pizza object with id: " + dto.getId());

    Optional<PizzaBean> pizza = pizzaRepository.findById(dto.getId());

    if (pizza.isPresent()) {
      dto.setDate(pizza.get().getDate());
      // pizzaValidator.validateSaveUpdate(dto);
      pizzaValidator.validateSlug(dto);
      BeanUtils.copyProperties(dto, pizza.get());
      logger.info("Pizza object with id: " + dto.getId() + " updated!");
      return mapToDTO(pizzaRepository.save(pizza.get()));
    }
    throw new IllegalArgumentException("Pizza object with id: " + dto.getId() + " don't exists!");
  }

  @Override
  @CacheEvict(value = "deletePizza", key = "#id", allEntries = true)
  public void delete(Long id) {
    logger.info("Delete Pizza object with id: " + id);

    Optional<PizzaBean> pizza = pizzaRepository.findById(id);

    if (pizza.isPresent()) {
      pizzaRepository.delete(pizza.get());
      logger.info("Pizza object with id: " + id + " deleted!");
    } else {
      throw new IllegalArgumentException("Pizza object for delete with id: " + id + " don't exists!");
    }
  }

  @Override
  public List<PizzaDTO> getByName(String name) {
    List<PizzaBean> pizzas = pizzaRepository.getByName(name);
    return pizzas.stream().map(this::mapToDTO).collect(Collectors.toList());
  }

  private PizzaDTO mapToDTO(PizzaBean bean) {
    PizzaDTO dto = new PizzaDTO(bean);
    return dto;
  }

}
