package com.store.service.impl;

import com.store.entity.User;
import com.store.repository.UserRepository;
import com.store.service.UserService;
import com.wo.service.impl.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl extends BaseServiceImpl<User, Long> implements UserService {

  private Logger logger = LoggerFactory.getLogger(this.getClass());

  private UserRepository userRepository;

  public UserServiceImpl(UserRepository userRepository) {
    super();
    this.userRepository = userRepository;
  }

  @Override
  public User getByUsername(String username) {
    return userRepository.getByUsername(username);
  }

}
