package com.store.service;

import com.store.dto.PizzaDTO;
import com.store.dto.search.PizzaSearch;
import com.store.entity.PizzaBean;
import com.wo.dto.SearchResultDTO;
import com.wo.service.BaseService;
import com.wo.service.search.Pager;

import java.util.List;
import java.util.Set;

public interface PizzaService extends BaseService<PizzaBean, Long> {

  SearchResultDTO<PizzaDTO> searchDTO(PizzaSearch search, Pager pager, Set<String> ignoreFields);

  PizzaDTO getDTO(Long id);

  PizzaDTO saveDTO(PizzaDTO dto);

  PizzaDTO updateDTO(PizzaDTO dto);

  void delete(Long id);

  List<PizzaDTO> getByName(String name);
}
