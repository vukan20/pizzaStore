package com.store.service;

import com.store.entity.User;
import com.wo.service.BaseService;

public interface UserService extends BaseService<User, Long> {

  User getByUsername(String username);
}
