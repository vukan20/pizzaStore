package com.store.dto;

import com.store.entity.PizzaBean;
import com.wo.dto.BaseDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PizzaDTO extends BaseDTO {

  private String name;

  private String slug;

  private BigDecimal size;

  private BigDecimal price;

  private Date date;

  public PizzaDTO(PizzaBean bean) {
    super();
    BeanUtils.copyProperties(bean, this);
  }
}
