package com.store.dto.search;

import com.wo.dto.search.ISearchDTO;
import com.wo.service.search.CompareMode;
import com.wo.service.search.DJSCompare;
import com.wo.service.search.DJSDate;
import com.wo.service.search.DateRoundingMode;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class PizzaSearch implements ISearchDTO {

  @DJSCompare(path = "id", mode = CompareMode.EQUAL)
  private Long id;

  @DJSCompare(path = "name", mode = CompareMode.LIKE_LR)
  private String name;

  @DJSCompare(path = "slug", mode = CompareMode.LIKE_LR)
  private String slug;

  @DJSCompare(path = "size", mode = CompareMode.GREATER_THAN_OR_EQUAL_TO)
  private BigDecimal sizeFrom;

  @DJSCompare(path = "size", mode = CompareMode.LESS_THAN_OR_EQUAL_TO)
  private BigDecimal sizeTo;

  @DJSCompare(path = "price", mode = CompareMode.GREATER_THAN_OR_EQUAL_TO)
  private BigDecimal priceFrom;

  @DJSCompare(path = "size", mode = CompareMode.LESS_THAN_OR_EQUAL_TO)
  private BigDecimal priceTo;

  @DJSDate(rounding = DateRoundingMode.START_OF_DAY)
  @DJSCompare(path = "date", mode = CompareMode.GREATER_THAN_OR_EQUAL_TO)
  private Date dateFrom;

  @DJSDate(rounding = DateRoundingMode.END_OF_DAY)
  @DJSCompare(path = "date", mode = CompareMode.LESS_THAN_OR_EQUAL_TO)
  private Date dateTo;

}