package com.store.entity;

import com.store.entity.enums.Role;
import com.wo.entity.BaseEntity;
import com.wo.exception.ErrMessages;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static org.hibernate.id.enhanced.TableGenerator.*;

@Entity
@Data
@Table(name = "st_user")
@GenericGenerator(strategy = "enhanced-table", name = BaseEntity.TABLE_GENERATOR_NAME, parameters = {
        @org.hibernate.annotations.Parameter(name = TABLE_PARAM, value = BaseEntity.COUNTER_TABLE_NAME),
        @org.hibernate.annotations.Parameter(name = VALUE_COLUMN_PARAM, value = BaseEntity.COUNTER_VALUE_COLUMN_NAME),
        @org.hibernate.annotations.Parameter(name = INCREMENT_PARAM, value = BaseEntity.COUNTER_ALLOCATION_SIZE),
        @org.hibernate.annotations.Parameter(name = SEGMENT_VALUE_PARAM, value = "user_next"),
        @org.hibernate.annotations.Parameter(name = SEGMENT_COLUMN_PARAM, value = BaseEntity.COUNTER_PK_COLUMN_NAME)})
public class User extends BaseEntity {

  @NotNull(message = ErrMessages.REQUIRED_FIELD + " - first_name")
  @Column(name = "first_name", nullable = false)
  private String firstName;

  @NotNull(message = ErrMessages.REQUIRED_FIELD + " - last_name")
  @Column(name = "last_name", nullable = false)
  private String lastName;

  @NotNull(message = ErrMessages.REQUIRED_FIELD + " - username")
  @Column(name = "username", nullable = false)
  private String username;

  @NotNull(message = ErrMessages.REQUIRED_FIELD + " - password")
  @Column(name = "password", nullable = false)
  private String password;

  @Column(name = "email")
  private String email;

  @Enumerated(EnumType.STRING)
  @Column(name = "role", nullable = false)
  private Role role;

}
