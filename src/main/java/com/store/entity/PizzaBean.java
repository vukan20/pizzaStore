package com.store.entity;

import com.wo.entity.BaseEntity;
import com.wo.exception.ErrMessages;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

import static org.hibernate.id.enhanced.TableGenerator.*;

@Entity
@Data
@Table(name = "st_pizza")
@GenericGenerator(strategy = "enhanced-table", name = BaseEntity.TABLE_GENERATOR_NAME, parameters = {
        @org.hibernate.annotations.Parameter(name = TABLE_PARAM, value = BaseEntity.COUNTER_TABLE_NAME),
        @org.hibernate.annotations.Parameter(name = VALUE_COLUMN_PARAM, value = BaseEntity.COUNTER_VALUE_COLUMN_NAME),
        @org.hibernate.annotations.Parameter(name = INCREMENT_PARAM, value = BaseEntity.COUNTER_ALLOCATION_SIZE),
        @org.hibernate.annotations.Parameter(name = SEGMENT_VALUE_PARAM, value = "st_pizza_next"),
        @org.hibernate.annotations.Parameter(name = SEGMENT_COLUMN_PARAM, value = BaseEntity.COUNTER_PK_COLUMN_NAME)})
public class PizzaBean extends BaseEntity {

  @NotNull(message = ErrMessages.REQUIRED_FIELD + " - name")
  @Column(name = "name", nullable = false)
  private String name;

  @NotNull(message = ErrMessages.REQUIRED_FIELD + " - slug")
  @Column(name = "slug", nullable = false, unique = true)
  private String slug;

  @NotNull(message = ErrMessages.REQUIRED_FIELD + " - size")
  @Column(name = "size", nullable = false)
  private BigDecimal size;

  @NotNull(message = ErrMessages.REQUIRED_FIELD + " - price")
  @DecimalMin(value = "0.01", message = ErrMessages.MINIMAL_PRICE)
  @Column(name = "price", nullable = false)
  private BigDecimal price;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date")
  private Date date;

}
