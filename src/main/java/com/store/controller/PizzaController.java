package com.store.controller;

import com.store.dto.PizzaDTO;
import com.store.dto.search.PizzaSearch;
import com.store.entity.PizzaBean;
import com.store.service.PizzaService;
import com.wo.dto.SearchResultDTO;
import com.wo.service.search.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "pizzas")
public class PizzaController {

  private PizzaService pizzaService;

  @Autowired
  public PizzaController(PizzaService pizzaService) {
    this.pizzaService = pizzaService;
  }

  @PreAuthorize("permitAll()")
  @GetMapping
  public List<PizzaBean> getAll() {
    return pizzaService.getAll();
  }

  @PreAuthorize("permitAll()")
  @GetMapping(value = "/search")
  public SearchResultDTO<PizzaDTO> search(@RequestBody PizzaSearch search, Pager pager) {
    return pizzaService.searchDTO(search, pager, null);
  }

  @GetMapping(value = "/{id}")
  public PizzaDTO getDTO(@PathVariable Long id) {
    return pizzaService.getDTO(id);
  }

  @PostMapping
  public PizzaDTO save(@RequestBody PizzaDTO dto) {
    return pizzaService.saveDTO(dto);
  }

  @PutMapping(value = "/{id}")
  public PizzaDTO update(@RequestBody PizzaDTO dto, @PathVariable Long id) {
    dto.setId(id);
    return pizzaService.updateDTO(dto);
  }

  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable Long id) {
    pizzaService.delete(id);
  }

  @GetMapping(value = "/getByName/{name}")
  public List<PizzaDTO> getByName(@PathVariable String name) {
    return pizzaService.getByName(name);
  }

}
