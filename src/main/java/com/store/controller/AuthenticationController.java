package com.store.controller;

import com.store.dto.UserDTO;
import com.store.entity.User;
import com.store.security.TokenUtils;
import com.store.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@RestController
@RequestMapping(value = "auth")
public class AuthenticationController extends BaseController {

  @Autowired
  private UserService userService;

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private UserDetailsService userDetailsService;

  @Autowired
  private TokenUtils tokenUtils;

  @PreAuthorize("permitAll()")
  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity authenticateUser(@RequestBody UserDTO dto) {
    User user = userService.getByUsername(dto.getUsername());
    if (user == null) {
      throw new UsernameNotFoundException("User '" + dto.getUsername() + "' not found");
    }

    UserDetails userDetails1 = org.springframework.security.core.userdetails.User
            .withUsername(dto.getUsername())
            .password(user.getPassword())
            .authorities(Collections.emptyList())
            .accountExpired(false)
            .accountLocked(false)
            .credentialsExpired(false)
            .disabled(false)
            .build();

    // Perform the authentication
    //UserDetails userDetails1 = userDetailsService.loadUserByUsername(dto.getUsername());
    System.out.println(userDetails1.getAuthorities());
    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
    Authentication authentication = authenticationManager.authenticate(authenticationToken);
    SecurityContextHolder.getContext().setAuthentication(authentication);
    try {
      // Reload user details so we can generate token
      UserDetails userDetails = userDetailsService.loadUserByUsername(dto.getUsername());
      return ResponseEntity.ok(tokenUtils.generateToken(userDetails));
    } catch (UsernameNotFoundException e) {
      return ResponseEntity.notFound().build();
    }
  }

}
