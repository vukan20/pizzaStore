package com.store.repository;

import com.store.entity.PizzaBean;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PizzaRepository extends CrudRepository<PizzaBean, Long> {

  List<PizzaBean> getByName(String name);

  Boolean existsBySlugAndIdNot(String slug, Long id);

  Boolean existsBySlug(String slug);

}
