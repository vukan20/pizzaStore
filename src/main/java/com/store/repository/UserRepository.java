package com.store.repository;

import com.store.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

  User getByUsername(String username);

}
