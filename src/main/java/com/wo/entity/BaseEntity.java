package com.wo.entity;

import com.wo.exception.ErrMessages;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@MappedSuperclass
public class BaseEntity implements Serializable {

  public static final String TABLE_GENERATOR_NAME = "ID_GEN";

  public static final String COUNTER_TABLE_NAME = "table_counter";

  public static final String COUNTER_PK_COLUMN_NAME = "TABLE_NAME";

  public static final String COUNTER_VALUE_COLUMN_NAME = "COUNTER_VALUE";

  public static final String COUNTER_ALLOCATION_SIZE = "1";

  @Id
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "ID_GEN")
  @NotNull(message = ErrMessages.REQUIRED_FIELD + " - id")
  protected Long id;

  @Version
  private Integer version;

  public BaseEntity() {
    super();
  }

  @Override
  public String toString() {
    return "BaseEntity{" + "id=" + id + ", version=" + version + '}';
  }
}
