package com.wo.dto;

import com.wo.entity.BaseEntity;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

@Data
public class BaseDTO implements Serializable {

  private Long id;

  private Integer version;

  public BaseDTO() {
    super();
  }

  public BaseDTO(BaseEntity baseEntity) {
    BeanUtils.copyProperties(baseEntity, this);
  }

  @Override
  public String toString() {
    return "BaseDTO{" + "id=" + id + ", version=" + version + '}';
  }
}
