package com.wo.dto.search;

public interface ISearchDTO {

  String FIELD_SUFIX_FROM = "From";

  String FIELD_SUFIX_TO = "To";

  String FIELD_SUFIX_LIKE = "Like";

  String FIELD_SUFIX_LIKE_R = "LikeR";

  String FIELD_SUFIX_LIKE_LR = "LikeLR";

  String FIELD_SUFIX_IS_NULL = "IsNull";

  String FIELD_SUFIX_IN = "In";

  String FIELD_SUFIX_SUB_FORM = "SF";
}
