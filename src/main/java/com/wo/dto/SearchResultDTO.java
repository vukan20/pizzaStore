package com.wo.dto;

import com.wo.entity.BaseEntity;
import com.wo.service.search.SearchResult;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

@Data
public class SearchResultDTO<T extends BaseDTO> {

  private List<T> results = new ArrayList<>();

  protected Integer count;

  protected Long total;

  public SearchResultDTO() {
    super();
  }

  public SearchResultDTO(SearchResult<? extends BaseEntity> searchResult) {
    BeanUtils.copyProperties(searchResult, this, "results");
  }

  public SearchResultDTO(List<T> results, Long total) {
    setResults(results);
    setTotal(total);
  }

}
