package com.wo.service;

import com.wo.dto.search.ISearchDTO;
import com.wo.entity.BaseEntity;
import com.wo.service.search.Pager;
import com.wo.service.search.SearchResult;

import java.io.Serializable;
import java.util.List;

public interface BaseService<T extends BaseEntity, ID extends Serializable> {

  T save(T entity);

  T get(ID id);

  List<T> getAll();

  void delete(ID id);

  void delete(Iterable<T> entities);

  SearchResult<T> search(ISearchDTO searchForm, Pager pager);

}
