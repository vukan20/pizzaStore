package com.wo.service.impl;

import com.wo.dto.search.ISearchDTO;
import com.wo.entity.BaseEntity;
import com.wo.service.BaseService;
import com.wo.service.search.Pager;
import com.wo.service.search.PredicateGenerator;
import com.wo.service.search.SearchResult;
import com.wo.service.search.SearchUtils;
import org.hibernate.query.criteria.internal.OrderImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Transactional
public abstract class BaseServiceImpl<T extends BaseEntity, ID extends Serializable> implements BaseService<T, ID> {

  protected Logger logger = LoggerFactory.getLogger(this.getClass());

  protected Class<T> entityClass;

  @Autowired
  protected CrudRepository<T, ID> repository;

  @PersistenceContext
  protected EntityManager em;

  @Value("${large.dataset.warning.threshold}")
  private Integer warningThreshold;

  @Autowired
  @Qualifier("DJSPredicateGenerator")
  private PredicateGenerator predicateGenerator;

  @PostConstruct
  private void setUp() {
    ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
    entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
  }

  @Override
  public T save(T entity) {
    em.detach(entity);
    return repository.save(entity);
  }

  @Override
  public T get(ID id) {
    if (id == null) {
      throw new IllegalArgumentException("Required param 'ID' not present");
    }

    Optional<T> bean = repository.findById(id);

    if (!bean.isPresent()) {
      throw new NullPointerException("Bean does not exists for given ID:" + id + " " + entityClass.getName());
    }

    return bean.get();
  }

  @Override
  public List<T> getAll() {
    List<T> entities = (List<T>) repository.findAll();
    if (entities == null) {
      entities = new ArrayList<>();
    }
    return entities;
  }

  @Override
  public void delete(ID id) {
    Optional<T> entity = repository.findById(id);
    if (entity != null) {
      repository.delete(entity.get());
    } else {
      throw new EntityNotFoundException("No object found with the id #" + id);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public void delete(Iterable<T> entities) {
    for (T entity : entities) {
      delete((ID) entity.getId());
    }
  }

  /**
   * Generise predikate na osnovu prosledjenog SearchForm objekta. Rekurzivno prolazi kroz sve ugnjezdene SearchForm
   * objekte i generise predikate i za njih. Moguce je promeniti ponasanje generisanja predikata za odredjena polja. Da
   * bi se to postiglo treba u konkretnoj implementaciji servisa promeniti neke od handleXxxXxx() metoda. Napomena:
   * promena ponasanja je moguca trenutno samo za polja root search forme. Ne primenjuje se nad poljima ugnjezdenih
   * formi. Polja za koja ne zelimo da se generise predikat automatski oznacavamo modifier-om "transient"
   */
  @SuppressWarnings("unchecked")
  @Override
  public SearchResult<T> search(ISearchDTO searchForm, Pager pager) {

    CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
    CriteriaQuery<Object> criteriaQuery = criteriaBuilder.createQuery();
    final Root<T> root = criteriaQuery.from(entityClass);

    // List of all predicates taht will be applied in "where" part
    // ========================================
    ArrayList<Predicate> predicates = new ArrayList<>();

    // Process all of fields in current class
    // and generates predicates for all of them
    // Generated predicates are added to "predicates" list
    // ========================================
    predicateGenerator.generatePredicates(predicates, criteriaBuilder, criteriaQuery, root, searchForm);

    // Execute query
    // ========================================

    // From
    criteriaQuery.select(root);

    // Where
    Predicate[] predArray = new Predicate[predicates.size()];
    for (int i = 0; i < predArray.length; i++) {
      predArray[i] = predicates.get(i);
    }
    criteriaBuilder.and(predArray);
    criteriaQuery.where(criteriaBuilder.and(predArray));

    // Order by
    List<Order> orders = generateOrderBy(pager, root);
    if (orders != null && !orders.isEmpty()) {
      criteriaQuery.orderBy(orders);
    }

    Long totalCount = null;

    Query qry = em.createQuery(criteriaQuery);

    if (pager != null && pager.getLimit() != null && pager.getPage() != null) {

      qry.setFirstResult(pager.getLimit() * (pager.getPage() - 1));
      qry.setMaxResults(pager.getLimit());

      CriteriaBuilder qb = em.getCriteriaBuilder();
      CriteriaQuery<Long> cq = qb.createQuery(Long.class);
      cq.select(qb.count(cq.from(entityClass)));
      cq.where(criteriaBuilder.and(predArray));

      totalCount = em.createQuery(cq).getSingleResult();
    }

    // Execute query and get all data
    SearchResult<T> result = new SearchResult<>(qry.getResultList(), totalCount);

    Integer resultSize = result.getResults().size();
    if (warningThreshold != null && resultSize > warningThreshold) {
      logger.warn(
              "Large dataset fetched from database. Size: " + resultSize + ", service: " + getClass().getSimpleName());
    }

    return result;
  }

  protected List<Order> generateOrderBy(Pager pager, Root<T> root) {
    List<Order> orders = new ArrayList<>();
    if (pager != null) {
      String orderBy = pager.getSorting();
      if (orderBy != null) {
        // sort always on one column (ascending is default)
        // for descending minus sign - will be prefixed to the value
        String colName;
        boolean isAscending = true;

        String[] parts = orderBy.split(":");
        colName = parts[0];

        if (parts.length > 1) {
          isAscending = !parts[1].equalsIgnoreCase("desc");
        }
        OrderImpl o = new OrderImpl(SearchUtils.parsePath(colName, root), isAscending);
        orders.add(o);
      }
    }
    return orders;
  }


}
