package com.wo.service.search;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.wo.dto.search.ISearchDTO;
import com.wo.exception.SystemException;
import com.wo.util.ClassFieldUtil;
import com.wo.util.DateUtil;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import javax.persistence.criteria.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Component("DJSPredicateGenerator")
public class DJSPredicateGenerator implements PredicateGenerator, ApplicationContextAware {

  ApplicationContext applicationContext;

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }

  @Override
  public void generatePredicates(List<Predicate> predicates,
                                 CriteriaBuilder criteriaBuilder,
                                 CriteriaQuery<?> criteriaQuery,
                                 Path<?> path,
                                 ISearchDTO searchForm) {


    Class<? extends ISearchDTO> sfClass = searchForm.getClass();
    List<Field> fields = ClassFieldUtil.getEntityFields(sfClass, true, true);

    for (Field field : fields) {
      ReflectionUtils.makeAccessible(field);
      Object value = ReflectionUtils.getField(field, searchForm);
      if (value != null) {
        DJSCompare annCompare = field.getAnnotation(DJSCompare.class);
        DJSMultiStringCompare annMultiCompare = field.getAnnotation(DJSMultiStringCompare.class);
        DJSContains annContains = field.getAnnotation(DJSContains.class);
        DJSSubSF annSubSF = field.getAnnotation(DJSSubSF.class);
        DJSIgnore annIgnore = field.getAnnotation(DJSIgnore.class);

        if (annCompare != null) {
          handleCompare(predicates, annCompare, field, criteriaBuilder, path, value);
        } else if (annMultiCompare != null) {
          handleSearchMulti(predicates, annMultiCompare, criteriaBuilder, path, value.toString());
        } else if (annContains != null) {
          handleSearchContains(predicates, field, annContains, criteriaBuilder, criteriaQuery, path, value);
        } else if (annSubSF != null) {
          generatePredicates(predicates, criteriaBuilder, criteriaQuery, parsePath(annSubSF.path(), path), (ISearchDTO) value);
        } else if (annIgnore != null) {
          // do nothing.
        } else {
          handlePredicateByFieldName(predicates, field, criteriaBuilder, criteriaQuery, path, value);
        }
      }
    }
    DJSSearchHandlerClass searchHandlerClassAnn = sfClass.getAnnotation(DJSSearchHandlerClass.class);
    if (searchHandlerClassAnn != null) {
      Class<? extends DJSearchHandler> searchHandlerClass = searchHandlerClassAnn.value();
      DJSearchHandler handler = applicationContext.getBean(searchHandlerClass);
      handler.generatePredicates(searchForm, predicates, criteriaBuilder, criteriaQuery, path);
    }
  }

  private void handleCompare(List<Predicate> predicates, DJSCompare annCompare, Field field, CriteriaBuilder criteriaBuilder, Path<?> path, Object value) {

    if (value instanceof Date) {
      value = processDateAnn(field, (Date) value);
    }

    List<Predicate> preds = new ArrayList<>();
    String[] paths = annCompare.path();

    for (String p : paths) {
      Path<?> fieldPath = parsePath(p, path);
      Predicate predicate;
      switch (annCompare.mode()) {
        case LIKE:
          predicate = criteriaBuilder.like((Path<String>) fieldPath, value.toString());
          break;
        case LIKE_R:
          predicate = criteriaBuilder.like((Path<String>) fieldPath, value.toString().concat("%"));
          break;
        case LIKE_LR:
          predicate = criteriaBuilder.like((Path<String>) fieldPath, "%".concat(value.toString()).concat("%"));
          break;
        case EQUAL:
          predicate = criteriaBuilder.equal(fieldPath, value);
          break;
        case LESS_THAN:
          predicate = criteriaBuilder.lessThan((Path<Comparable>) fieldPath, (Comparable) value);
          break;
        case GREATER_THAN:
          predicate = criteriaBuilder.greaterThan((Path<Comparable>) fieldPath, (Comparable) value);
          break;
        case LESS_THAN_OR_EQUAL_TO:
          predicate = criteriaBuilder.lessThanOrEqualTo((Path<Comparable>) fieldPath, (Comparable) value);
          break;
        case GREATER_THAN_OR_EQUAL_TO:
          predicate = criteriaBuilder.greaterThanOrEqualTo((Path<Comparable>) fieldPath, (Comparable) value);
          break;
        case NOT_EQUAL:
          predicate = criteriaBuilder.notEqual(fieldPath, value);
          break;
        case IS_NULL:
          if (Boolean.TRUE.equals(value)) {
            predicate = criteriaBuilder.isNull(fieldPath);
          } else {
            predicate = criteriaBuilder.isNotNull(fieldPath);
          }
          break;
        case IS_NOT_NULL:
          if (Boolean.TRUE.equals(value)) {
            predicate = criteriaBuilder.isNotNull(fieldPath);
          } else {
            predicate = criteriaBuilder.isNull(fieldPath);
          }
          break;
        case NOT_LIKE:
          predicate = criteriaBuilder.notLike((Path<String>) fieldPath, value.toString());
          break;
        case IN: {
          if (value instanceof Collection) {
            if (!((Collection) value).isEmpty()) {
              predicate = criteriaBuilder.and(fieldPath.in((Collection) value));
            } else {
              predicate = criteriaBuilder.and(fieldPath.isNull());
            }
          } else if (value instanceof Expression) {
            predicate = criteriaBuilder.and(fieldPath.in((Expression) value));
          } else {
            predicate = criteriaBuilder.and(fieldPath.in(value));
          }
        }
        break;
        case NOT_IN:
          Collection values = (Collection) value;
          Predicate notInPredicate;

          if (values.isEmpty()) {
            notInPredicate = fieldPath.isNull();
          } else {
            notInPredicate = fieldPath.in(values).not();
          }

          predicate = criteriaBuilder.and(notInPredicate);
          break;

        default:
          throw new SystemException("Can not handle mode " + annCompare.mode() + " for " + field);
      }
      if (predicate != null) {
        preds.add(predicate);
      }
    }

    if (preds.size() == 1) {
      predicates.add(preds.get(0));
    }

    if (preds.size() > 1) {
      Predicate[] predArr = new Predicate[preds.size()];
      for (int i = 0; i < preds.size(); i++) {
        predArr[i] = preds.get(i);
      }
      Predicate p;
      if (annCompare.operation().equals(CompareOperation.AND)) {
        p = criteriaBuilder.and(predArr);
      } else {
        p = criteriaBuilder.or(predArr);
      }
      predicates.add(p);
    }
  }

  /**
   * @param criteriaBuilder
   * @param path
   * @param value
   * @return
   */
  private void handleSearchMulti(List<Predicate> predicates, DJSMultiStringCompare sma, CriteriaBuilder criteriaBuilder, Path<?> path, String value) {

    List<String> searchTerms = new ArrayList<>();

    if (sma.splitIntoWords()) {
      Splitter.on(CharMatcher.anyOf(sma.splitChars())).split(value)
              .forEach(searchTerms::add);
    } else {
      searchTerms.add(value);
    }

    int numOfOrs = sma.paths().length * searchTerms.size();

    Predicate[] ors = new Predicate[numOfOrs];

    int i = 0;
    for (String term : searchTerms) {
      for (String p : sma.paths()) {
        Path<String> root = (Path<String>) parsePath(p, path);
        switch (sma.mode()) {
          case LIKE:
            ors[i++] = criteriaBuilder.like(root, term.replaceAll("\\*", "%"));
            break;
          case LIKE_R:
            ors[i++] = criteriaBuilder.like(root, term.replaceAll("\\*", "%").concat("%"));
            break;
          case LIKE_LR:
            ors[i++] = criteriaBuilder.like(root, "%".concat(term.replaceAll("\\*", "%")).concat("%"));
            break;
          case EQUAL:
            ors[i++] = criteriaBuilder.equal(root, term);
            break;
          default:
            throw new SystemException("Not supported mode " + sma.mode());
        }
      }
    }

    predicates.add(criteriaBuilder.or(ors));
  }

  // Contains generise po sablonu "SELECT entitet FROM entiteti WHERE entitet IN (SELECT subQueryType.mappedBy FROM subQueryType WHERE xxxContains...):
  private void handleSearchContains(List<Predicate> predicates, Field field, DJSContains ann, CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Path<?> path, Object value) {

    if (!(value instanceof ISearchDTO)) {
      throw new SystemException("Field " + field.getName() + " annotated with DJSContains must be subclass of ISearchDTO + ");
    }
    Class<?> subQueryType = ann.subQueryType();
    String mappedBy = ann.mappedBy();

    // Generisanje IN (subquery) predikata rekurzivno generisuci podupit (subquery)
    // na osnovu sadrzaja xxxContains field-a
    Subquery<?> subquery = criteriaQuery.subquery(Long.class);
    CriteriaBuilder.In in = criteriaBuilder.in(path);
    in.value(subquery);

    // Podupit
    Root<?> sqEmp = subquery.from(subQueryType);
    subquery.select(sqEmp.get(mappedBy).get("id"));
    List<Predicate> subPreds = new ArrayList<>();
    generatePredicates(subPreds, criteriaBuilder, criteriaQuery, sqEmp, (ISearchDTO) value);
    Predicate[] predArr = new Predicate[subPreds.size()];
    subPreds.toArray(predArr);
    subquery.where(criteriaBuilder.and(predArr));

    predicates.add(in);
  }

  private void handlePredicateByFieldName(List<Predicate> predicates, Field field, CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Path<?> path, Object value) {

    if (value instanceof Date) {
      value = processDateAnn(field, (Date) value);
    }

    String fieldName = field.getName();
    if (fieldName.endsWith(ISearchDTO.FIELD_SUFIX_FROM)) {
      fieldName = fieldName.substring(0, fieldName.indexOf(ISearchDTO.FIELD_SUFIX_FROM));
      predicates.add(criteriaBuilder.greaterThan(path.get(fieldName), (Comparable) value));

    } else if (fieldName.endsWith(ISearchDTO.FIELD_SUFIX_TO)) {
      fieldName = fieldName.substring(0, fieldName.indexOf(ISearchDTO.FIELD_SUFIX_TO));
      predicates.add(criteriaBuilder.lessThan(path.get(fieldName), (Comparable) value));

    } else if (fieldName.endsWith(ISearchDTO.FIELD_SUFIX_IN)) {
      fieldName = fieldName.substring(0, fieldName.indexOf(ISearchDTO.FIELD_SUFIX_IN));
      predicates.add(criteriaBuilder.and(path.get(fieldName).in(value)));

    } else if (fieldName.endsWith(ISearchDTO.FIELD_SUFIX_IS_NULL)) {
      fieldName = fieldName.substring(0, fieldName.indexOf(ISearchDTO.FIELD_SUFIX_IS_NULL));
      if ((Boolean) value) {
        predicates.add(criteriaBuilder.isNull(path.get(fieldName)));
      } else {
        predicates.add(criteriaBuilder.isNotNull(path.get(fieldName)));
      }

    } else if (fieldName.endsWith(ISearchDTO.FIELD_SUFIX_LIKE)) {
      fieldName = fieldName.substring(0, fieldName.indexOf(ISearchDTO.FIELD_SUFIX_LIKE));
      predicates.add(criteriaBuilder.like(path.get(fieldName), value.toString().replaceAll("\\*", "%")));

    } else if (fieldName.endsWith(ISearchDTO.FIELD_SUFIX_LIKE_LR)) {
      fieldName = fieldName.substring(0, fieldName.indexOf(ISearchDTO.FIELD_SUFIX_LIKE_LR));
      predicates.add(criteriaBuilder.like(path.get(fieldName), "%".concat(value.toString().replaceAll("\\*", "%")).concat("%")));

    } else if (fieldName.endsWith(ISearchDTO.FIELD_SUFIX_LIKE_R)) {
      fieldName = fieldName.substring(0, fieldName.indexOf(ISearchDTO.FIELD_SUFIX_LIKE_R));
      predicates.add(criteriaBuilder.like(path.get(fieldName), value.toString().replaceAll("\\*", "%").concat("%")));

    } else if (fieldName.endsWith(ISearchDTO.FIELD_SUFIX_SUB_FORM)) {
      fieldName = fieldName.substring(0, fieldName.indexOf(ISearchDTO.FIELD_SUFIX_SUB_FORM));
      generatePredicates(predicates, criteriaBuilder, criteriaQuery, path.get(fieldName), (ISearchDTO) value);

    } else {
      equals(predicates, criteriaBuilder, path.get(fieldName), value);
    }
  }

  private void equals(List<Predicate> predicates, CriteriaBuilder criteriaBuilder, Path<?> path, Object value) {
    predicates.add(criteriaBuilder.equal(path, value));
  }

  private Path<?> parsePath(String path, Path<?> root) {
    Path<?> retPath = root;
    String[] pathElements = path.split("\\.");
    for (String pathElement : pathElements) {
      retPath = retPath.get(pathElement);
    }
    return retPath;
  }


  private Date processDateAnn(Field f, Date value) {
    DJSDate dateAnn = f.getAnnotation(DJSDate.class);
    if (dateAnn != null) {
      switch (dateAnn.rounding()) {
        case START_OF_DAY:
          return DateUtil.beginningOfDay(value);
        case END_OF_DAY:
          return DateUtil.endOfDay(value);
        case BEGINNING_OF_NEXT_DAY:
          return DateUtil.nextDay(value);
        default:
          return value;
      }
    } else {
      return value;
    }
  }

}
