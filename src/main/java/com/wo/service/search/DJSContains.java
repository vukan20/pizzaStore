package com.wo.service.search;

import com.wo.entity.BaseEntity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DJSContains {

  String mappedBy();

  Class<? extends BaseEntity> subQueryType();

}
