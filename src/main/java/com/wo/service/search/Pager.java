package com.wo.service.search;

import lombok.Data;

import java.io.Serializable;

@Data
public class Pager implements Serializable {

  public static final String SORT_SEPARATOR = ":";
  public static final String DIR_ASC = "asc";
  public static final String DIR_DESC = "desc";
  private Integer page;
  private Integer limit;
  private String sorting;

}
