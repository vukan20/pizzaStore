package com.wo.service.search;

import com.wo.dto.search.ISearchDTO;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.util.List;

public interface PredicateGenerator {

  void generatePredicates(List<Predicate> predicates,
                          CriteriaBuilder criteriaBuilder,
                          CriteriaQuery<?> criteriaQuery,
                          Path<?> path,
                          ISearchDTO searchForm);

}
