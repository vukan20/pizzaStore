package com.wo.service.search;

import javax.persistence.criteria.Path;

public class SearchUtils {

  public static Path<?> parsePath(String path, Path<?> root) {
    Path<?> retPath = root;
    String[] pathElements = path.split("\\.");
    for (String pathElement : pathElements) {
      retPath = retPath.get(pathElement);
    }
    return retPath;
  }

}
