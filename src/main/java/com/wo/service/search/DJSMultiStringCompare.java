package com.wo.service.search;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DJSMultiStringCompare {

  String[] paths();

  String splitChars() default " ,:;-";

  boolean splitIntoWords() default false;

  CompareMode mode() default CompareMode.EQUAL;


}
