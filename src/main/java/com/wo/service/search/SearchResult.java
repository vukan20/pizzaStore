package com.wo.service.search;

import com.wo.entity.BaseEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SearchResult<T extends BaseEntity> {

  private List<T> results = new ArrayList<>();

  private Long total;

  public SearchResult() {
    super();
  }

  public SearchResult(SearchResult<T> searchResult) {
    results = searchResult.getResults();
    total = searchResult.getTotal();
  }

  public SearchResult(List<T> results, Long total) {
    setResults(results);
    setTotal(total);
  }

}
