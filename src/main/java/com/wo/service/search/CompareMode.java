package com.wo.service.search;

public enum CompareMode {

  LIKE,
  LIKE_R,
  LIKE_LR,
  EQUAL,
  LESS_THAN,
  LESS_THAN_OR_EQUAL_TO,
  GREATER_THAN,
  GREATER_THAN_OR_EQUAL_TO,
  NOT_EQUAL,
  IS_NULL,
  IS_NOT_NULL,
  IN,
  NOT_IN,
  NOT_LIKE
}
