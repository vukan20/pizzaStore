package com.wo.service.search;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DJSCompare {

  String[] path();

  CompareMode mode() default CompareMode.EQUAL;

  CompareOperation operation() default CompareOperation.AND;

}
