package com.wo.service.search;

public enum DateRoundingMode {

  NONE, START_OF_DAY, END_OF_DAY, BEGINNING_OF_NEXT_DAY

}
