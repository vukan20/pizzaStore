package com.wo.util;

import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class ClassFieldUtil implements ReflectionUtils.FieldCallback {

  private boolean skipTransient;
  private boolean skipStatic;

  private List<Field> fields = new ArrayList<>();

  private ClassFieldUtil(boolean skipTransient, boolean skipStatic) {
    this.skipTransient = skipTransient;
    this.skipStatic = skipStatic;
  }

  public static List<Field> getEntityFields(Class<?> classs, boolean skipTransient, boolean skipStatic) {
    ClassFieldUtil fcbk = new ClassFieldUtil(skipTransient, skipStatic);
    ReflectionUtils.doWithFields(classs, fcbk);
    return fcbk.getFields();
  }

  @Override
  public void doWith(Field field) {
    int modifiers = field.getModifiers();

    boolean skip = skipTransient && Modifier.isTransient(modifiers) || skipStatic && Modifier.isStatic(modifiers);

    if (!skip) {
      fields.add(field);
    }
  }

  List<Field> getFields() {
    return fields;
  }
}
