package com.wo.util;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class DateUtil {

  private DateUtil() {
  }

  public static Date endOfDay(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    c.set(Calendar.HOUR, 23);
    c.set(Calendar.MINUTE, 59);
    c.set(Calendar.SECOND, 59);
    c.set(Calendar.MILLISECOND, 999);
    return c.getTime();
  }

  public static Date beginningOfDay(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    c.set(Calendar.HOUR, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    return c.getTime();
  }

  public static Date nextDay(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    c.set(Calendar.HOUR, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    c.add(Calendar.DAY_OF_MONTH, 1);
    return c.getTime();
  }

  public static Date endOfYear(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    c.set(Calendar.MONTH, 11);
    c.set(Calendar.DAY_OF_MONTH, 31);
    c.set(Calendar.HOUR, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    return c.getTime();
  }

  public static Date startOfYear(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    c.set(Calendar.MONTH, 0);
    c.set(Calendar.DAY_OF_MONTH, 1);
    c.set(Calendar.HOUR, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    return c.getTime();
  }

  public static Date endOfYearForYear(int year) {
    Calendar c = Calendar.getInstance();
    c.set(Calendar.MONTH, 11);
    c.set(Calendar.DAY_OF_MONTH, 31);
    c.set(Calendar.HOUR, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    c.set(Calendar.YEAR, year);
    return c.getTime();
  }

  public static Date startOfYearForYear(int year) {
    Calendar c = Calendar.getInstance();
    c.set(Calendar.MONTH, 0);
    c.set(Calendar.DAY_OF_MONTH, 1);
    c.set(Calendar.HOUR, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    c.set(Calendar.YEAR, year);
    return c.getTime();
  }

  public static Date addYears(Date date, int years) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.add(Calendar.YEAR, years);
    return cal.getTime();
  }


  public static String formatiranDatum(Date date) {
    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.");
    return sdf.format(date);
  }

  public static long getRadniDaniRazlika(LocalDate startDate, LocalDate endDate) {
    EnumSet<DayOfWeek> weekend = EnumSet.of(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);
    List<LocalDate> list = new ArrayList<>();

    LocalDate start = startDate;
    while (!start.isAfter(endDate)) {
      list.add(start);
      start = start.plus(1, ChronoUnit.DAYS);
    }

    long numberOfDays = list.stream().filter(d -> !weekend.contains(d.getDayOfWeek())).count();

    return numberOfDays;
  }

}
