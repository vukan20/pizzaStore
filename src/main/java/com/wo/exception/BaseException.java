package com.wo.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * Base exception.
 */
public abstract class BaseException extends RuntimeException {

  public static final String DEFAULT_ERROR_CODE = "global.error.general";
  public static final String DEFAULT_ERROR_MESSAGE = "Error occured";

  /**
   * Error code for client side translation.
   */
  protected final String errCode;

  /**
   *
   */
  protected final List<String> msgParams = new ArrayList<>();


  /**
   * Additional data that will be sent to the client in response
   */
  protected Object payload;

  /**
   * Constructor.
   */
  public BaseException() {
    super(DEFAULT_ERROR_MESSAGE);
    this.errCode = DEFAULT_ERROR_CODE;
  }

  /**
   * Constructor.
   */
  public BaseException(Exception cause) {
    super(DEFAULT_ERROR_MESSAGE, cause);
    this.errCode = DEFAULT_ERROR_CODE;
  }

  /**
   * Constructor.
   *
   * @param errCode error code for frontend
   */
  public BaseException(String errCode, Object... msgParams) {
    super(DEFAULT_ERROR_MESSAGE);
    this.errCode = errCode;
    setMsgParams(msgParams);
  }


  /**
   * Constructor.
   *
   * @param errCode error code for frontend
   * @param cause   exception
   */
  public BaseException(String errCode, Exception cause, Object... msgParams) {
    super(DEFAULT_ERROR_MESSAGE, cause);
    this.errCode = errCode;
    setMsgParams(msgParams);
  }

  /**
   * Constructor.
   *
   * @param errCode error code for frontend
   * @param errMsg  error message for logging
   */
  public BaseException(String errCode, String errMsg, Object... msgParams) {
    super(errMsg);
    this.errCode = errCode;
    setMsgParams(msgParams);
  }

  /**
   * Constructor.
   *
   * @param errCode error code for frontend
   * @param errMsg  error message for logging
   */
  public BaseException(String errCode, String errMsg, Exception cause, Object... msgParams) {
    super(errMsg, cause);
    this.errCode = errCode;
    setMsgParams(msgParams);
  }

  protected void setMsgParams(Object[] params) {
    for (Object param : params) {
      msgParams.add(param.toString());
    }
  }

  public String getErrCode() {
    return errCode;
  }

  public List<String> getMsgParams() {
    return msgParams;
  }

  public Object getPayload() {
    return payload;
  }

  public void setPayload(Object payload) {
    this.payload = payload;
  }
}
