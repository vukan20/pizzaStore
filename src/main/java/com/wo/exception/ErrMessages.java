package com.wo.exception;

public class ErrMessages {

  public static final String REQUIRED_FIELD = "This field is required";

  public static final String INVALID_SLUG_FORMAT = "Invalid slug format";

  public static final String UNIQUE_SLUG = "Slug already in use";

  public static final String MINIMAL_PRICE = "Minimal price is 0.01";

  private ErrMessages() {
  }
}
