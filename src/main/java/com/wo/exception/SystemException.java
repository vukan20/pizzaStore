package com.wo.exception;

/**
 * Exception that represents unexpected system errors that do not require special . No special message will be shown to
 * user. Throwing code should specify message that will be logged into file. Also, this exception should be used to wrap
 * exceptions that third party libs throw.
 */
public class SystemException extends BaseException {

  public static final String DEFAULT_SYSTEM_ERROR_CODE = "global.error.general";
  public static final String DEFAULT_SYSTEM_ERROR_MESSAGE = "System error occured";

  /**
   * @param cause
   */
  public SystemException(Exception cause) {
    super(DEFAULT_SYSTEM_ERROR_CODE, DEFAULT_SYSTEM_ERROR_MESSAGE, cause);
  }

  /**
   * @param message
   */
  public SystemException(String message) {
    super(DEFAULT_SYSTEM_ERROR_CODE, message);
  }

  /**
   * @param message
   * @param cause
   */
  public SystemException(String message, Exception cause) {
    super(DEFAULT_SYSTEM_ERROR_CODE, message, cause);
  }
}
