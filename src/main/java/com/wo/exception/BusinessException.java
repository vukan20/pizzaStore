package com.wo.exception;


public class BusinessException extends BaseException {


  public BusinessException(String msgCode, String message, Exception cause, Object... msgParams) {
    super(msgCode, message, cause, msgParams);
  }

  public BusinessException(String msgCode, Object... msgParams) {
    super(msgCode, msgParams);
  }
}
